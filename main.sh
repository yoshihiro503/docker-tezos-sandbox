#!/bin/bash
set -ex
./src/bin_node/tezos-sandboxed-node.sh 1 --connections 1 &
shopt -s expand_aliases
eval `./src/bin_client/tezos-init-sandboxed-client.sh 1`
which tezos-client
tezos-client rpc get /chains/main/blocks/head/metadata
tezos-activate-alpha
tezos-client list known addresses
while true; do ./do_bake.sh; sleep 10; done
