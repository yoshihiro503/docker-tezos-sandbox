localhost only tezos sandbox environment with docker

## How to Use

### Start local sandbox node

```console
docker run tezos-sandbox
```


### Login the server

```console
docker exec -it <the_process_id> bash
```

```console
opam@1321c32b1bee:~/opam-repository/tezos$ eval `./src/bin_client/tezos-init-sandboxed-client.sh 1`
```
