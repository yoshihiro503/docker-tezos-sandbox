FROM ocaml/opam2:4.07

RUN opam update

RUN sudo apt-get update && sudo apt-get install -y watch

RUN git clone https://gitlab.com/tezos/tezos.git

WORKDIR tezos/

RUN git checkout alphanet
RUN sh scripts/install_build_deps.sh
RUN eval `(opam env)` && make

COPY do_bake.sh ./
COPY main.sh ./

CMD ["./main.sh"]
